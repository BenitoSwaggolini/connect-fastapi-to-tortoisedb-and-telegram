from datetime import datetime, timedelta

from app.telegram.mybot.dao import Message, Chat, MyBotUser

from ._conftest import *


@pytest.mark.anyio
async def test_send_and_save_message(client: AsyncClient):
    """Test message send and save"""
    await Chat.all().delete()
    test_chat = await Chat.create(id=726318959, hash='1', type='2007',
                                  full_name="There can be no peace in the world until the military power of Japan "
                                            "isn't "
                                            "destroyed!")
    data = {"text": 'text'}

    response = await client.post(f'/messages/{test_chat.id}/send', params=data)
    assert response.status_code == 200
    assert (await Message.get(chat=test_chat)).text == data["text"]


@pytest.mark.anyio
async def test_get_messages(client: AsyncClient):
    """Test message get"""
    await Chat.all().delete()
    await Message.all().delete()
    await MyBotUser.all().delete()

    test_chat: Chat = await Chat.create(id=2006, hash='3', type='private', full_name="Test_chat")

    test_user1 = await MyBotUser.create(id=1, hash='1', first_name=2, username='Andrey', language_code='Taiwan',
                                        is_bot=True)
    test_user2 = await MyBotUser.create(id=2, hash='1', first_name='woman', username='Selfedu', language_code='Chinese',
                                        is_bot=True)

    oldest_test_message = await Message.create(id=1, chat=test_chat, author=test_user1, text='first',
                                               created_at=datetime.now() - timedelta(days=5))
    recent_test_message = await Message.create(id=2, chat=test_chat, author=test_user2, text='second',
                                               created_at=datetime.now())

    response = await client.get(f'/messages/{test_chat.id}', params={"limit": 100, "offset": 0})
    assert response.status_code == 200
    assert len(response.json()) == 2
    new_response_msg, old_response_message = response.json()

    assert new_response_msg["id"] == recent_test_message.id
    assert new_response_msg["text"] == recent_test_message.text
    assert new_response_msg["author"] == {key: value for key, value in dict(test_user2).items() if key != 'hash'}
    assert new_response_msg["chat"] == {key: value for key, value in dict(test_chat).items() if key != 'hash'}

    assert old_response_message["id"] == oldest_test_message.id
    assert old_response_message["text"] == oldest_test_message.text
    assert old_response_message["author"] == {key: value for key, value in dict(test_user1).items() if key != 'hash'}
    assert old_response_message["chat"] == {key: value for key, value in dict(test_chat).items() if key != 'hash'}

    response2 = await client.get(f'/messages/{test_chat.id}', params={"limit": 1, "offset": 0})
    assert response2.status_code == 200
    assert len(response2.json()) == 1

    response3 = await client.get(f'/messages/{test_chat.id}', params={"limit": 2, "offset": 1})
    assert response3.status_code == 200
    assert len(response3.json()) == 1


@pytest.mark.anyio
async def test_get_chats(client: AsyncClient):
    await Chat.all().delete()
    chat1 = await Chat.create(id=0, hash=1, full_name='2', type="4")
    chat2 = await Chat.create(id=1, hash=11, full_name='22', type="44")

    response = await client.get('/chats', params={"limit": 2, "offset": 0})

    assert response.status_code == 200
    response = response.json()

    assert len(response) == 2
    assert response[0] == {key: value for key, value in dict(chat1).items() if key != 'hash'} and response[1] == {
        key: value for key, value in dict(chat2).items() if key != 'hash'}

    response2 = await client.get('/chats', params={"limit": 1, "offset": 0})
    assert response2.status_code == 200
    assert len(response2.json()) == 1

    response3 = await client.get('/chats', params={"limit": 2, "offset": 1})
    assert response3.status_code == 200
    assert len(response3.json()) == 1


@pytest.mark.anyio
async def test_get_users(client: AsyncClient):
    await MyBotUser.all().delete()

    user1 = await MyBotUser.create(id=1, hash='1', first_name=2, username='Andrey', language_code='Taiwan', is_bot=True)
    user2 = await MyBotUser.create(id=2, hash='1', first_name='woman', username='Selfedu', language_code='Chinese',
                                   is_bot=False)

    response = await client.get('/users', params={"limit": "2", "offset": 0})
    assert response.status_code == 200
    assert len(response.json()) == 2

    data = ({key: value for key, value in dict(user2).items() if key != 'hash'},
            {key: value for key, value in dict(user1).items() if key != 'hash'})
    assert response.json()[0] in data and response.json()[1] in data


@pytest.mark.anyio
async def test_get_current_user(client: AsyncClient):
    await MyBotUser.all().delete()
    test_user = await MyBotUser.create(id=1, hash='1776', first_name='test_name', username='Herald',
                                       language_code='Taiwan',
                                       is_bot=True)
    response = await client.get(f'/users/{test_user.id}')

    assert dict(response.json()) == {key: value for key, value in dict(test_user).items() if key != 'hash'}

    wrong_response = await client.get(f'/users/fjsdfjsdfdiojqweo')
    assert wrong_response.status_code == 422


@pytest.mark.anyio
async def test_get_current_message(client: AsyncClient):
    await Chat.all().delete()
    await MyBotUser.all().delete()
    test_user = await MyBotUser.create(id=1, hash='1776', first_name='test_name', username='Herald',
                                       language_code='Taiwan',
                                       is_bot=True)
    test_chat = await Chat.create(id=1, hash='1', type='2007',
                                  full_name="zero")
    test_message = await Message.create(id=1, author=test_user, chat=test_chat, text='32q423',
                                        created_at=datetime.now())
    response = await client.get(f'/messages/{test_chat.id}/{test_message.id}')
    assert response.status_code == 200

    response = response.json()[0]
    assert all((response["id"] == test_message.id, response["text"] == test_message.text,
                response["author"] == {key: value for key, value in dict(test_user).items() if key != 'hash'},
                response["chat"] == {key: value for key, value in dict(test_chat).items() if key != 'hash'}))
