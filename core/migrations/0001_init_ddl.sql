CREATE TABLE IF NOT EXISTS MyBotUser
(
    id            BIGINT PRIMARY KEY NOT NULL ,
    hash          VARCHAR(999) NOT NULL,
    first_name    VARCHAR(255) NULL,
    username      VARCHAR(255) NULL,
    language_code VARCHAR(255) NULL,
    is_bot        BOOLEAN      NOT NULL

);

CREATE TABLE IF NOT EXISTS Chat
(
    id    BIGINT PRIMARY KEY NOT NULL ,
    hash  VARCHAR(999) NOT NULL,
    full_name VARCHAR(255) NOT NULL ,
    type  VARCHAR(255) NULL

);

CREATE TABLE IF NOT EXISTS Message
(
    true_id  UUID PRIMARY KEY NOT NULL ,
    id BIGINT NOT NULL,
    author_id  BIGINT,
    chat_id    BIGINT,
    text       TEXT,
    created_at TIMESTAMPTZ NOT NULL,
    FOREIGN KEY (author_id) REFERENCES MyBotUser (id) ON DELETE CASCADE,
    FOREIGN KEY (chat_id) REFERENCES Chat (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS "user" (
    "id" UUID NOT NULL  PRIMARY KEY,
    "username" VARCHAR(255) NOT NULL UNIQUE,
    "hashed_password" VARCHAR(255) NOT NULL,
    "disabled" BOOL NOT NULL
);