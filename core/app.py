from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from starlette.websockets import WebSocket
from app.telegram.mybot import api
from app.telegram.mybot.deps import broadcaster
from app.auth.api import app as auth_app
app = FastAPI(title='Service')

# routers
app.include_router(api.app, tags=["telegram"])
app.include_router(auth_app, )
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.websocket('/messages/ws')
async def websocket_update_messages(websocket: WebSocket):
    await websocket.accept()
    async with broadcaster.subscribe(channel="chatroom") as subscriber:
        async for event in subscriber:
            await websocket.send_json(data=event.message)
