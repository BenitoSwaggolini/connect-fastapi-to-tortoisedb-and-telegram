from tortoise.contrib.pydantic import pydantic_model_creator
from .dao import User
from pydantic import BaseModel

UserPD = pydantic_model_creator(User, exclude=('hashed_password',))


class TokenData(BaseModel):
    access_token: str | None
    token_type: str | None
    username: str
