from app.telegram.mybot.deps import dp
from aiogram import types
from app.telegram.mybot.services import save_tg_message


@dp.message_handler()
async def catch_telegram_message(message: types.Message):
    await save_tg_message(tg_msg=message, tg_user=message.from_user, tg_chat=message.chat)
