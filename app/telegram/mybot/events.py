from .deps import broadcaster
from .dao import Message, signals
from .models import MessagePD


@signals.post_save(Message)
async def send_new_saved_message_to_broadcaster(*args, **kwargs):
    msg: MessagePD = MessagePD.from_orm(args[1])
    await broadcaster.publish(channel="chatroom", message=msg.json())
