from datetime import datetime
from tortoise.contrib.pydantic import PydanticModel, pydantic_model_creator
from .dao import Chat, MyBotUser

MyBotUserPD = pydantic_model_creator(MyBotUser, exclude=('hash',))
ChatPD = pydantic_model_creator(Chat, exclude=('hash',))


class MessagePD(PydanticModel):
    id: int
    created_at: datetime
    author: MyBotUserPD
    chat: ChatPD
    text: str
