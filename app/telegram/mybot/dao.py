from tortoise import models, fields, signals


class MyBotUser(models.Model):
    """Telegram user default model"""
    id = fields.BigIntField(pk=True, description="Telegram user id", generated=False)
    hash = fields.CharField(max_length=999)
    first_name = fields.CharField(max_length=255, description="Telegram user first_name", null=True)
    username = fields.CharField(max_length=255, description="Telegram user tag", null=True)
    language_code = fields.CharField(max_length=255, null=True)
    is_bot = fields.BooleanField()

    def __str__(self):
        return f"User {self.first_name}"


class Chat(models.Model):
    """Telegram chat model"""
    id = fields.BigIntField(pk=True, description="Telegram chat id", generated=False)
    hash = fields.CharField(max_length=999)
    full_name = fields.CharField(max_length=255)
    type = fields.CharField(max_length=255, description="Chat mode", null=True)

    def __str__(self):
        return f"Chat {self.id}"


class Message(models.Model):
    """Telegram message model"""
    true_id = fields.UUIDField(pk=True, description='True database unique id')
    id = fields.BigIntField(description='telegram message id', generated=False)
    author = fields.ForeignKeyField('models.MyBotUser', on_delete=fields.CASCADE)
    chat = fields.ForeignKeyField('models.Chat', on_delete=fields.CASCADE)
    text = fields.TextField(description='Telegram message text', )
    created_at = fields.DatetimeField(description='Message creation datetime')

    class Meta:
        ordering = ["-created_at"]

    def __str__(self):
        return f"Message from {self.author} in {self.chat}"
