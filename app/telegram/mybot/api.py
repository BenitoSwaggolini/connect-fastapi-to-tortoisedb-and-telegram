from typing import List

from .deps import app, bot
from app.telegram.mybot.handlers.commands import *
from .events import *
from .models import *
from .services import save_tg_message
from app.auth.services import User, Depends, get_current_active_user


@app.get('/users')
async def get_users(limit: int, offset: int, _: User = Depends(get_current_active_user)) -> List[MyBotUserPD]:
    """Get users with limit and set params"""
    return await MyBotUserPD.from_queryset(MyBotUser.all().limit(limit).offset(offset))


@app.get('/users/{user_id}')
async def get_current_user(user_id: int, _: User = Depends(get_current_active_user)) -> MyBotUserPD | None:
    """Get user by his id"""
    return await MyBotUserPD.from_queryset_single(MyBotUser.get_or_none(id=user_id))


@app.get('/chats')
async def get_chats(limit: int, offset: int, _: User = Depends(get_current_active_user)) -> List[ChatPD]:
    """Chat List"""
    return await ChatPD.from_queryset(Chat.all().limit(limit).offset(offset))


@app.get('/chats/{chat_id}')
async def get_current_chat(chat_id: int, _: User = Depends(get_current_active_user)) -> ChatPD:
    """Get chat by its id"""
    return await ChatPD.from_orm(Chat.get_or_none(id=chat_id))


@app.get('/messages/{chat_id}')
async def get_messages(chat_id: int, limit: int, offset: int, _: User = Depends(get_current_active_user)):
    """display messages from chat with pagination"""
    queryset = await Message.filter(chat_id=chat_id).select_related('chat', 'author').limit(limit).offset(
        offset).order_by('-created_at')
    return [MessagePD.from_orm(data) for data in queryset]


@app.get('/messages/{chat_id}/{message_id}')
async def get_current_message(chat_id: int, message_id: int, _: User = Depends(get_current_active_user)) -> List[
    MessagePD]:
    """Return current message from chat"""
    queryset = await Message.filter(chat_id=chat_id, id=message_id).select_related('chat', 'author')
    return [MessagePD.from_orm(data) for data in queryset]


@app.post('/messages/{chat_id}/send')
async def send_message(chat_id: int, text: str, _: User = Depends(get_current_active_user)) -> MessagePD:
    """Send message to current chat"""
    response = await bot.send_message(chat_id=chat_id, text=text)
    msg = await save_tg_message(tg_user=response.from_user,
                                tg_chat=response.chat,
                                tg_msg=response
                                )
    return MessagePD.from_orm(msg)
