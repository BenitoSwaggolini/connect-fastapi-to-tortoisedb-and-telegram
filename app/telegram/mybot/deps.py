from fastapi.routing import APIRouter
from tortoise import connections

from core.settings import settings
from aiogram import Dispatcher, Bot, types
from broadcaster import Broadcast

app = APIRouter()
bot: Bot = Bot(token=settings.TELEGRAM_TOKEN)
dp: Dispatcher = Dispatcher(bot)

broadcaster = Broadcast(url=settings.REDIS_CONNECTION)


@app.on_event("startup")
async def on_startup():
    """In this method, we connect our bot to ethernet using ngrok and webhooks"""
    await broadcaster.connect()
    await bot.delete_webhook(drop_pending_updates=True)
    await bot.set_webhook(url=settings.WEBHOOK_URL + '/webhook')


@app.on_event('shutdown')
async def on_shutdown():
    await connections.close_all()
    await broadcaster.disconnect()


@app.post(f'/webhook')
async def bot_webhook(update: dict):
    """In this method, we receive user's messages from webhook post requests"""
    Dispatcher.set_current(dp)
    Bot.set_current(bot)
    await dp.process_update(update=types.Update(**update))
