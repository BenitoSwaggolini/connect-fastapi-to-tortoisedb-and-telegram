from hashlib import md5

from app.telegram.mybot.dao import Message, MyBotUser, Chat
from aiogram import types


async def save_tg_message(tg_user: types.User, tg_chat: types.Chat, tg_msg: types.Message) -> Message:
    """User"""
    user_hash = md5(fr"{tg_user.first_name}-{tg_user.username}-{tg_user.language_code}".encode("utf8")).hexdigest()
    user: MyBotUser = await MyBotUser.get_or_none(id=tg_user.id)
    if not user:
        user = await MyBotUser.create(hash=user_hash, **tg_user.to_python())
    elif user.hash != user_hash:
        await MyBotUser.filter(id=tg_user.id).update(hash=user_hash, first_name=tg_user.first_name,
                                                     username=tg_user.username,
                                                     language_code=tg_user.language_code)

    """Chat"""
    chat_hash = md5(fr"{tg_chat.title}-{tg_chat.type}".encode("utf8")).hexdigest()
    chat_fullname = tg_chat.full_name
    chat = await Chat.get_or_none(id=tg_chat.id)

    if not chat:
        chat = await Chat.create(id=tg_chat.id, hash=chat_hash, full_name=chat_fullname, type=tg_chat.type)
    elif chat.hash != chat_hash:
        await Chat.filter(id=tg_chat.id).update(hash=chat_hash, full_name=chat_fullname, type=tg_chat.type)

    """Create message"""
    return await Message.create(id=tg_msg.message_id,
                                author=user,
                                chat=chat,
                                text=tg_msg.text,
                                created_at=tg_msg.date)
