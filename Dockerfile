FROM python:3.11-alpine

WORKDIR /

COPY . .

RUN pip install -r requirements.txt

ENTRYPOINT ["python", "main.py"]
EXPOSE 8000